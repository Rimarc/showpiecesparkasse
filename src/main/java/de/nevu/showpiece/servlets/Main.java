package de.nevu.showpiece.servlets;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.Date;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import de.nevu.showpiece.logic.AppLogic;
import de.nevu.showpiece.logic.ToDo;
import de.nevu.showpiece.logic.Transaction;
import de.nevu.showpiece.logic.User;

/**
 * Servlet implementation class Main
 */
public class Main extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public Main() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub

		AppLogic AL=(AppLogic) request.getServletContext().getAttribute("APPLOGIC");

		PrintWriter out = response.getWriter();
		response.setContentType("text/html");

		out.println("<h1>ISparkasse</h1>");


		String action=request.getParameter("action");

		String UserName=request.getParameter("name");

		if(AL.doesUserExist(UserName)==false)
		{
			AL.addUser(UserName);
			out.println("Neuer Nutzer Angelegt! <br>");

		}

		out.println("Aktueller Benutzer: "+UserName+"<br>");

		if(action.equals("send_money"))
		{
			try {
				User ThisUser = AL.getUser(UserName);
				User reciver=AL.getUser(request.getParameter("reciver"));
				double amount=Double.parseDouble(request.getParameter("amount"));
				reciver.money+=amount;
				ThisUser.money-=amount;
				out.println(ThisUser.name +" hat "+amount+" zu "+reciver.name+" transferiert<br>");
				ThisUser.Transactions.add(new Transaction(UserName, reciver.name, amount, new Date()));

			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

			/*			AL.addTodoToUser(UserName,
					new ToDo(UserName, request.getParameter("caption"), 
							request.getParameter("description"), 1)
					);
			out.println("New ToDo added!<br>");*/
		}

		try {

			User user= AL.getUser(UserName);
			out.println("Kontostand: "+user.money+"<br>");


			//out.println(AL.serializeToString(AL.getUser(UserName)));


		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		out.println("<form action='Main'>");
		out.println("Reciver:");

		out.println("<select name='reciver'>");

		for(User u : AL.GetAllUsers())
		{
			out.println("<option value='"+u.name+"'>"+u.name+"</option>");
		}
		out.println("</select><br>");

		out.println("Amount:<input type='text' name='amount'>");
		out.println("<input type='hidden' name='name' value='"+UserName+"'>");
		out.println("<input type='hidden' name='action' value='send_money' />");
		out.println("<input type='submit' name='action' value='transferieren' />");
		out.println("</form><br>");

		out.println("<a href='history.jsp?name="+UserName+"'>Transaaktionen</a><br>");
		out.print("<a href='javascript:history.back()'>Zur�ck</a><br>");
		out.print("<a href='/Main?name="+UserName+"&action=none'>Aktualisieren</a><br>");
		out.print("<a href='/'>Home</a><br>");




	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}

package de.nevu.showpiece.logic;

import java.util.List;

import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;

import com.google.inject.Inject;

import de.nevu.showpiece.data.DataAccess;



public class AppLogic {
	
	@Inject
	public AppLogic(DataAccess dao, SerializerService SerializerService) {
		this.dao = dao;
		this.serializer=SerializerService;
	}

	private DataAccess  dao;
	private SerializerService serializer;
	public Boolean doesUserExist(String name)
	{
		return dao.userExists(name);
	}
	
	
	public User getUser(String name) throws Exception
	{
		return dao.GetUser(name);
	}

	public void addUser(String name) {
		dao.addNewUser(name, 1000);
	}
	
	public String serializeToString(Object o)
	{
		return serializer.SerializePOJOtoString(o);
	}

	public void shutDown() {
		dao.close();
	}
	
	public List<User> GetAllUsers()
	{
		return dao.GetAllUsers();
	}
	
	public void payInterest() {
		System.out.println("PayDay!");
		for(User u : dao.GetAllUsers())
		{
			u.money=u.money*1.03;
		}
	}
	
}

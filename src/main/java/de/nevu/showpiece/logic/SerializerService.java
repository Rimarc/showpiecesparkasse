package de.nevu.showpiece.logic;

public interface SerializerService {

	public String SerializePOJOtoString(Object o);
}

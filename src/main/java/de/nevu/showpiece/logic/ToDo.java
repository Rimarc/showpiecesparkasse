package de.nevu.showpiece.logic;

import org.mongodb.morphia.annotations.Embedded;

@Embedded
public class ToDo {
	
	//Constructor for Morphia
	public ToDo()
	{
	}
	public ToDo(String user, String caption, String description, int priority) {
		this.user = user;
		this.caption = caption;
		this.description = description;
		Priority = priority;
	}
	public String user;
	public String caption;
	public String description;
	public int Priority;
	
}

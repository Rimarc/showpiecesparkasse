package de.nevu.showpiece.logic;

import java.util.ArrayList;
import java.util.List;

import org.mongodb.morphia.Datastore;
import org.mongodb.morphia.annotations.Embedded;
import org.mongodb.morphia.annotations.Id;
import org.mongodb.morphia.mapping.Mapper;
import org.mongodb.morphia.query.Query;
import org.bson.types.ObjectId;

public class User {
	

	//this Constructor is mainly for Morphia
	public User()
	{
	}
	
	public User(String name, double monney)
	{
		this.name = name;
		this.money=monney;
		this.Transactions=new ArrayList<Transaction>();
	}

	public double money;
	public String name;
	public List<Transaction> Transactions;

}

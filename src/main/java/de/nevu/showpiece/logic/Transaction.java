package de.nevu.showpiece.logic;

import java.util.*;

import org.mongodb.morphia.annotations.Embedded;

@Embedded
public class Transaction {
	
	//Constructor for Morphia
	public Transaction()
	{
	}
	public Transaction(String sender, String reciver, double amount, Date date) {
		this.sender = sender;
		this.reciver = reciver;
		this.amount = amount;
		this.date = date;
	}
	public String sender;
	public String reciver;
	public double amount;
	public Date date;
	
}

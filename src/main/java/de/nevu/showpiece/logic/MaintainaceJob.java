package de.nevu.showpiece.logic;

import org.quartz.Job;
import org.quartz.JobDataMap;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;

public class MaintainaceJob implements Job {
	public static String MAP_ID="AppLogic";
	@Override
	public void execute(JobExecutionContext context) throws JobExecutionException {
		
		((AppLogic)context.getJobDetail().getJobDataMap().get(MAP_ID)).payInterest();

	}

}

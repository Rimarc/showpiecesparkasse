package de.nevu.showpiece.data;

import java.util.ArrayList;
import java.util.List;

import de.nevu.showpiece.logic.ToDo;
import de.nevu.showpiece.logic.User;

public interface DataAccess {
	public void addNewUser(String name, double monney);
	
	public Boolean userExists(String name);
	
	public User GetUser(String name) throws Exception;

	public List<User> GetAllUsers();
	
	public void close();

}

package de.nevu.showpiece.data;

import com.google.inject.AbstractModule;

import de.nevu.showpiece.logic.JSONSerializer;
import de.nevu.showpiece.logic.SerializerService;

public class InjectorModul extends AbstractModule {

	@Override
	protected void configure() {
		bind(DataAccess.class).to(VolatileMemmoryImpl.class);
		//bind(DataAccess.class).to(RedisImpl.class);
		bind(SerializerService.class).to(JSONSerializer.class);
	}

	

}

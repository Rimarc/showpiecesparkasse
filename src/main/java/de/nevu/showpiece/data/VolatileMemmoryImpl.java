package de.nevu.showpiece.data;

import java.util.ArrayList;
import java.util.List;

import de.nevu.showpiece.logic.ToDo;
import de.nevu.showpiece.logic.User;

public class VolatileMemmoryImpl implements DataAccess {

	public VolatileMemmoryImpl() {
		Users=new ArrayList<User>();
		Users.add(new User("Mark", 1000));
	}

	List<User> Users;

	public Boolean userExists(String name) {
		for(User user: Users)
		{
			if(user.name.equals(name))
			{
				return true; 
			}
		}
		return false;
	}


	public User GetUser(String name) throws Exception {
		for(User user: Users)
		{
			if(user.name.equals(name))
			{
				return user; 
			}
		}
		throw new Exception("Wanted to get User that doesnt exist");
	}

	@Override
	public void close() {
		return;
		
	}

	@Override
	public void addNewUser(String name, double money) {
		Users.add(new User(name, money));
		
	}


	@Override
	public List<User> GetAllUsers() {
		// TODO Auto-generated method stub
		return Users;
		
	}

}

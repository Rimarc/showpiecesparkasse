package de.nevu.showpiece.data;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import javax.servlet.annotation.WebListener;

import org.quartz.JobDetail;

import com.google.inject.Guice;
import com.google.inject.Injector;
import de.nevu.showpiece.logic.AppLogic;
import de.nevu.showpiece.logic.MaintainaceJob;

import org.quartz.Scheduler;
import org.quartz.SchedulerException;
import org.quartz.Trigger;
import org.quartz.impl.StdSchedulerFactory;
import static org.quartz.JobBuilder.*;
import static org.quartz.TriggerBuilder.*;
import static org.quartz.SimpleScheduleBuilder.*;

@WebListener
public class ContextListener implements ServletContextListener {

	@Override
	public void contextDestroyed(ServletContextEvent sce) {
		((AppLogic)sce.getServletContext().getAttribute("APPLOGIC")).shutDown();
	}

	@Override
	public void contextInitialized(ServletContextEvent sce) {
		Injector injector = Guice.createInjector(new InjectorModul());
		AppLogic AL=injector.getInstance(AppLogic.class);
		
		System.out.println("APPLOGIC initialized successfully");
		
		sce.getServletContext().setAttribute("APPLOGIC", AL);
		
		//Initialize the Quartzscheduler and Invoke some Maintanancefunction
		Scheduler scheduler;
		try {
			scheduler = StdSchedulerFactory.getDefaultScheduler();
			scheduler.start();
			
			
			
			JobDetail job = newJob(MaintainaceJob.class)
				      .withIdentity("job1", "group1")
				      .build();
			
			job.getJobDataMap().put(MaintainaceJob.MAP_ID, (Object)AL);
			

				  // Trigger the job to run now, and then repeat every 60 seconds
				  Trigger trigger = newTrigger()
				      .withIdentity("trigger1", "group1")
				      .startNow()
				      .withSchedule(simpleSchedule()
				              .withIntervalInSeconds(60)
				              .repeatForever())
				      .build();

				  // Tell quartz to schedule the job using our trigger
				  scheduler.scheduleJob(job, trigger);
				  
		} catch (SchedulerException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		
		
	}

}